import random
import pygame
from configuration import Configuration

class Pomme():
    def __init__(self):
        self.position = (0,0)
        self.couleur = Configuration.couleur_pomme
        self.positionner_aleatoirement()

    def positionner_aleatoirement(self):
        # positionner aleatoirement 
        self.position = (random.randint(0, Configuration.case_largeur-1)*Configuration.taille_grille, random.randint(0, Configuration.case_hauteur-1)*Configuration.taille_grille)

    # afficher pomme
    def afficher(self, surface):
        # taille_grille = taille carre
        carre = pygame.Rect((self.position[0], self.position[1]), (Configuration.taille_grille, Configuration.taille_grille))
        # dessiner sur surface la carre
        pygame.draw.rect(surface, self.couleur, carre)