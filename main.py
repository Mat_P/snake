import pygame
import random
import sys
from configuration import Configuration
from serpent import Serpent
from pomme import Pomme
from grille import Grille

def main():
    # init de pygame
    pygame.init()

    # fichier du meilleur score
    file = open("scores.txt", "r")
    lines = file.readlines()
    highscore = []
    
    for line in lines:
        highscore.append(int(line))

    meilleurScore = max(highscore)
    
    # liberer memoire fichier
    file.close()

    grille = Grille()

    # sync du jeu
    clock = pygame.time.Clock()

    surface = pygame.Surface(grille.taille())
    surface = surface.convert()
    grille.afficherGrille(surface)

    serpent = Serpent()
    pomme = Pomme()

    myfont = pygame.font.SysFont("arial", 21)

    while (True):
        # gameloop
        # framrate
        clock.tick(Configuration.framerate)
        # inpnputs
        serpent.gerer_inputs()
        # afficher ce quil se passe
        grille.afficherGrille(surface)
        serpent.deplacer()
        if serpent.tete_position() == pomme.position:
            serpent.taille += 1
            Configuration.score += 1
            pomme.positionner_aleatoirement()
        # on cree la surface
        serpent.afficher(surface)
        pomme.afficher(surface)
        # on pose la surface sur l'ecran avec blit
        grille.blit(surface, (0,0))
        score_text = myfont.render("Score actuelle: {}".format(Configuration.score), 1, Configuration.couleur_tableau)
        highestscore_text = myfont.render("Meilleur score: {}".format(meilleurScore), 1, Configuration.couleur_tableau)
        grille.blit(highestscore_text, (5,30))
        grille.blit(score_text, (5,10))
        pygame.display.update()

main()