class Configuration():
    score = 0
    # couleur en rgb
    couleur_serpent = (28, 131, 52)
    couleur_pomme = (255, 0, 0)
    framerate = 10
    # multiple de 2 pcq c'est un carre
    grille_largeur = 800
    grille_hauteur = 800
    # doit etre pair et different de 0
    taille_grille = 20
    case_largeur = grille_largeur / taille_grille
    case_hauteur = grille_hauteur / taille_grille
    # aide pour indiquer la direction, le positif de y est a -1 (va vers le haut)
    haut = (0,-1)
    bas = (0,1)
    gauche = (-1,0)
    droite = (1,0)
    couleur_primaire_case = (160, 221, 73)
    couleur_secondaire_case = (159, 159, 159)
    couleur_tableau = (0, 0, 0)