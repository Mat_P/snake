import random
import pygame
import sys
from configuration import Configuration

class Serpent():
    def __init__(self):
        self.taille = 1
        # chaque carre du serpent 10x10
        self.membres = [((Configuration.grille_largeur / 2), (Configuration.grille_hauteur / 2))]
        # random la direction
        self.direction = random.choice([Configuration.haut, Configuration.bas, Configuration.gauche, Configuration.droite])
        self.couleur = Configuration.couleur_serpent

    # get la position du premier element du serpent
    def tete_position(self):
        return self.membres[0]

    def changer_direction(self, direction):
        # si serpent > 1 et que direction contraire, return pour pas se tuer
        if self.taille > 1 and (direction[0]*-1, direction[1]*-1) == self.direction:
            return
        # si serpent = une case
        else:
            self.direction = direction

    def deplacer(self):
        # get tete du serpent
        tete_position = self.tete_position()
        x,y = self.direction
        prochaine_position = (((tete_position[0] + (x * Configuration.taille_grille)) % Configuration.grille_largeur), (tete_position[1] + (y * Configuration.taille_grille)) % Configuration.grille_hauteur)
        # est-ce que le serpent se mord la queue (3 cases minimum). les 2 premiers peuvent pas se toucher
        if len(self.membres) > 2 and prochaine_position in self.membres[2:]:
            self.reinitialiser()
        else:
            # ajout nouvelle tete
            self.membres.insert(0,prochaine_position)
            if len(self.membres) > self.taille:
                self.membres.pop()

    def reinitialiser(self):
        with open("scores.txt", "a") as f:
            f.write('{0}\n'.format(str(Configuration.score)))
        self.taille = 1
        self.membres = [((Configuration.grille_largeur / 2), (Configuration.grille_hauteur / 2))]
        self.direction = random.choice([Configuration.haut, Configuration.bas, Configuration.gauche, Configuration.droite])
        Configuration.score = 0
        

    def gerer_inputs(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.changer_direction(Configuration.haut)
                elif event.key == pygame.K_DOWN:
                    self.changer_direction(Configuration.bas)
                elif event.key == pygame.K_LEFT:
                    self.changer_direction(Configuration.gauche)
                elif event.key == pygame.K_RIGHT:
                    self.changer_direction(Configuration.droite)

    def afficher(self,surface):
        for position in self.membres:
            carre = pygame.Rect((position[0], position[1]), (Configuration.taille_grille,Configuration.taille_grille))

            pygame.draw.rect(surface, self.couleur, carre)