import pygame
import sys
from configuration import Configuration

class Grille():
    def __init__(self):
        # set taille de la fenetre
        self.grille = pygame.display.set_mode((Configuration.grille_largeur, Configuration.grille_hauteur))

    def taille(self):
        # get taille de la fenetre
        return self.grille.get_size()

    def blit(self, surface, position):
        # deposer la surface sur la fenetre
        self.grille.blit(surface, position)

    def afficherGrille(self, surface):
        for y in range(0, int(Configuration.case_hauteur)):   
            for x in range(0, int(Configuration.case_largeur)):
                carre = pygame.Rect((x*Configuration.taille_grille, y*Configuration.taille_grille), (Configuration.taille_grille,Configuration.taille_grille))
                # si pair carre couleur
                if (x+y)%2 == 0:
                    pygame.draw.rect(surface,Configuration.couleur_primaire_case, carre)
                # si impair carre autre couleur    
                else:
                    pygame.draw.rect(surface, Configuration.couleur_secondaire_case, carre)